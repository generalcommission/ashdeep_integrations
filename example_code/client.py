import json
from typing import Dict
import requests


class CmsClient:
    BASE_URL = f'https://data.cms.gov/provider-data/api'

    def get_data(self) -> Dict:
        url = f'{self.BASE_URL}/1/datastore/sql?query=[SELECT * FROM c9374743-7983-5f45-9c2a-04c2476d295a]'
        params = {

        }
        data = self._make_request(url, params)
        return data

    def get_data_from_file(self) -> Dict:
        # Open the orders.json file
        with open("Physicians.json") as file:
            # Load its content and make a new dictionary
            data = json.load(file)
        return data

    def _make_request(self, url: str, params: Dict) -> Dict:
        headers = {
            'Content-Type': 'application/json',
            'Cache-Control': 'no-cache'
        }
        response = requests.get(url=url, params=params, headers=headers)
        response.raise_for_status()
        return response.json()
