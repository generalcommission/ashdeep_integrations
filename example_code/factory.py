from extractor import PhysiciansExtractor
from client import CmsClient


def get_extractor() -> PhysiciansExtractor:
    client = CmsClient()
    extractor = PhysiciansExtractor(client)
    return extractor
