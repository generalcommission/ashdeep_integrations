import json
from typing import Dict, List, Union, Optional
from client import CmsClient
from dtos import Physician


class PhysiciansExtractor:
    def __init__(self, api_client: CmsClient):
        self.api_client = api_client

    def get_physicians(self) -> Union[List[Physician], Dict]:
        try:
            response = self.api_client.get_data()
            raw_data = response
            structured_data = self._get_structured_physicians(raw_data)
            data_response = {
                'data': structured_data
            }
            return data_response
        except Exception as ex:
            return {
                'errors': ex[0]
            }

    def physiciansbeta(self, firstname: str, lastname: str) -> Physician:
        try:
            response = self.api_client.get_data_from_file()
            search_result = [x for x in response if x['frst_nm'] == firstname and x['lst_nm'] == lastname]
            structured_data = self._get_structured_physicians(search_result)
            data_response = structured_data[0]
            return data_response
        except Exception as ex:
            return {
                'errors': ex[0]
            }

    def _get_physician_dto(self, physician: Dict) -> Physician:
        physician_dto = Physician(
            NPI=physician.get('NPI'),
            Ind_PAC_ID=physician.get('Ind_PAC_ID'),
            Ind_enrl_ID=physician.get('Ind_enrl_ID'),
            lst_nm=physician.get('lst_nm'),
            frst_nm=physician.get('frst_nm'),
            mid_nm=physician.get('mid_nm'),
            suff=physician.get('suff'),
            gndr=physician.get('gndr'),
            Cred=physician.get('Cred'),
            Med_sch=physician.get('Med_sch'),
            Grd_yr=physician.get('Grd_yr'),
            pri_spec=physician.get('pri_spec'),
            sec_spec_1=physician.get('sec_spec_1'),
            sec_spec_2=physician.get('sec_spec_2'),
            sec_spec_3=physician.get('sec_spec_3'),
            sec_spec_4=physician.get('sec_spec_4'),
            sec_spec_all=physician.get('sec_spec_all'),
            org_nm=physician.get('org_nm'),
            org_pac_id=physician.get('org_pac_id'),
            num_org_mem=physician.get('num_org_mem'),
            adr_ln_1=physician.get('adr_ln_1'),
            adr_ln_2=physician.get('adr_ln_2'),
            ln_2_sprs=physician.get('ln_2_sprs'),
            cty=physician.get('cty'),
            st=physician.get('st'),
            zip=physician.get('zip'),
            phn_numbr=physician.get('phn_numbr'),
            hosp_afl_1=physician.get('hosp_afl_1'),
            hosp_afl_lbn_1=physician.get('hosp_afl_lbn_1'),
            hosp_afl_2=physician.get('hosp_afl_2'),
            hosp_afl_lbn_2=physician.get('hosp_afl_lbn_2'),
            hosp_afl_3=physician.get('hosp_afl_3'),
            hosp_afl_lbn_3=physician.get('hosp_afl_lbn_3'),
            hosp_afl_4=physician.get('hosp_afl_4'),
            hosp_afl_lbn_4=physician.get('hosp_afl_lbn_4'),
            hosp_afl_5=physician.get('hosp_afl_5'),
            hosp_afl_lbn_5=physician.get('hosp_afl_lbn_5'),
            ind_assgn=physician.get('ind_assgn'),
            grp_assgn=physician.get('grp_assgn'),
            adrs_id=physician.get('adrs_id'),

        )
        return physician_dto

    def _get_structured_physicians(self, physicians: List) -> List[Physician]:
        physicians_list = [self._get_physician_dto(physician) for physician in physicians]
        return physicians_list
