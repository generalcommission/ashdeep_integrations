# README #
### What is this repository for? ###

* This repository will be for commiting and reviewing work done by Ashdeep on integrating datasources for General Commission.
### Project Description ###

* Build an integration for the physician data found at the following URL:
    <https://data.cms.gov/provider-data/dataset/mj5m-pzi6>

* We want the following query to work: 
```
physiciansBeta(firstname: String, lastname: String): [Physician]
```
* So we want the inputs to be the variables above. The flow should look like this:
```
Inputs (firstname or lastname) -> query the dataset for matching results -> Output(the schema provided in *physician_compare.graphql*)
```
* At the bottom of the previous link you will find the endpoint to query the dataset using SQL syntax:
```
​/provider-data​/api​/1​/datastore​/sql?query=[SELECT * FROM 43095f17-7bab-51a4-a6ab-ef045f416ec6];
```
* We likely need to update this query with the input variables. So something like:
```
"SELECT * FROM uuid WHERE frst_nm = firstname, lst_nm = lastname"
```
* The following are the columns you will find in the dataset and a short description:
```
{
    "mj5m-pzi6": {
        "description": "This file contains general information about individual eligible professionals (EPs) such as demographic information and Medicare quality program participation. This dataset is updated twice a month with the most current demographic information available at that time.",
        "schema": ["NPI", "Ind_PAC_ID", "Ind_enrl_ID", "lst_nm", "frst_nm", "mid_nm", "suff", "gndr", "Cred", "Med_sch", "Grd_yr", "pri_spec", "sec_spec_1", "sec_spec_2", "sec_spec_3", "sec_spec_4", "sec_spec_all", "org_nm", "org_pac_id", "num_org_mem", "adr_ln_1", "adr_ln_2", "ln_2_sprs", "cty", "st", "zip", "phn_numbr", "hosp_afl_1", "hosp_afl_lbn_1", "hosp_afl_2", "hosp_afl_lbn_2", "hosp_afl_3", "hosp_afl_lbn_3", "hosp_afl_4", "hosp_afl_lbn_4", "hosp_afl_5", "hosp_afl_lbn_5", "ind_assgn", "grp_assgn", "adrs_id"]
    }
}
```
* This document will help translate the columns in the schema to the target in physician_compare.graphql
    <https://data.cms.gov/provider-data/sites/default/files/data_dictionaries/DOC_Data_Dictionary.pdf>

* I've included in this repository a file called *physician_compare.graphql*, this file shows the schema we would like for our dataclasses. There is an example of a dataclass file (called dtos.py) in the example_code file
### Expected Deliverables ###
* **client.py**
    * The file responsible for making requests to the api endpoint
* **extractor.py**
    * The file responsible for taking the raw output of the api and translating it to our dataclasses
* **dtos.py**
    * The file that defines our dataclasses
* **factory.py**
    * The file that defines the function we can use to call your client/extractor in the rest of our codebase
*  **test_physician_compare_client.py**
    *  unit tests for the hospital_compare client
*  **test_physician_compare_extractor.py**
    *  unit tests for the hospital_compare extractor
* There are examples of each file in the example_code folder that are from a previous integration in our codebase. Your code may differ slightly because of the sql syntax but the examples should give you a basic idea of the structure

### Formatting ###

* Do your best to follow the formatting pattern in the example code
* Follow PEP8
* Please place all working code in a folder called physician_compare_integration

### Who do I talk to? ###

* Reach Andrew Noel in our Upwork chat at anytime with questions. Please feel free to provide feedback or suggestions on how the project could be more clear.